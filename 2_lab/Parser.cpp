#include "Parser.h"
#include <iostream>
#include <vector>
#include <sstream>
#include "WorkflowConstants.h"

using std::istream;
using std::string;
using std::stringstream;
using std::getline;
using kalinskymalinsky_WorkflowConstants::STANDART_DELIMETER;
using kalinskymalinsky_WorkflowConstants::UP_DESC_EDGE;
using kalinskymalinsky_WorkflowConstants::DOWN_DESC_EDGE;
using kalinskymalinsky_WorkflowConstants::DOWN_DESC_FLAG;
using std::vector;
using kalinskymalinsky_WorkflowConstants::ZERO;
using kalinskymalinsky_WorkflowConstants::MODULE_DESCRIPTION_DELIMETER;
using kalinskymalinsky_WorkflowConstants::STANDART_CONVEYOR_DELIMETER;

using kalinskymalinsky_WorkflowConstants::EXEPTION_MESSAGE;
using kalinskymalinsky_WorkflowConstants::Exeptions;

namespace kalinskymalinsky_DescParser {

    DescParser::DescParser( istream * inputWorkflowStream ) : inputWorkflowStream_(inputWorkflowStream){};
    DescParser::DescParser() : inputWorkflowStream_(NULL) {};
    Workflow * DescParser::parse() {

        vector<IWorker *> moduleArray;
        vector<int> conveyorArray;
        Workflow * workflow;

        skipToDesc(); // Comments before "desc"
        moduleArray = getModuleArray();
        conveyorArray = getConveyorArray();
        workflow = new Workflow(moduleArray, conveyorArray);
        return workflow;

    }
    void DescParser::skipToDesc( ) const { // Skip things before "desc"
                                           // Carriage will stay at the next line after "desc"
        string buffer;
        do {
            try {
                if ( !getline(*inputWorkflowStream_, buffer) ) {
                    throw Exeptions::INCORRECT_WORKFLOW;
                }
            }
            catch (Exeptions e){
                sendExeptionMessage(e);
                exit(1);
            }
        } while (buffer != UP_DESC_EDGE);
    }
    void DescParser::setInputWorkflow(istream & newInputWorkflow) {
         delete inputWorkflowStream_;
        inputWorkflowStream_ = &newInputWorkflow; //
    }
    int DescParser::getIndex(istream & inputLine) const {
        string buffer;
        int index = 0;

        try {
            if ( !getline(inputLine, buffer, STANDART_DELIMETER) ){
                throw Exeptions :: CANT_GET_INDEX;
            }
        }
        catch(Exeptions e) {
            sendExeptionMessage(e);
            exit(1);
        }
        try {
            index = atoi(buffer.c_str());
            if ( (index == 0) && ( (buffer[0] != ZERO) || (buffer.size() > 1) ) ) {
                throw Exeptions :: INCORRECT_INDEX;
            }
        }
        catch(Exeptions e) {
            sendExeptionMessage(e);
            exit(1);
        }
        return index;
    }
    string DescParser::getModuleDescription(istream & inputLine) const {
        string buffer;
        try {
            if (!getline(inputLine, buffer, STANDART_DELIMETER) || (buffer != MODULE_DESCRIPTION_DELIMETER)){ // This ignores "= "
                throw Exeptions :: INCORRECT_WORKFLOW;
            }
        }
        catch (Exeptions e) {
            sendExeptionMessage(e);
            exit(1);
        }

        getline(inputLine, buffer);
        return buffer;
    }
    vector<IWorker *> DescParser::getModuleArray() {
        int index = 0;
        stringstream * line;
        string buffer;
        string moduleDescription;
        vector<IWorker *> moduleArray;
        IWorker * module;

        while ( true ) {
            getline(*inputWorkflowStream_, buffer);
            if ( buffer == DOWN_DESC_EDGE) {
                break;
            }
            line = new stringstream(buffer);
            index = getIndex( *line );
            moduleDescription = getModuleDescription( *line );

            try {
                module = Workflow::getModule(moduleDescription);
                if (module == NULL) {
                    throw Exeptions :: UNINITIALIZED_MODULE;
                }
            }
            catch (Exeptions e) {
                sendExeptionMessage(e);
                exit(1);
            }

            if (index >= moduleArray.size()) {
                moduleArray.resize(index + 1);
            }
            // moduleArray.insert(moduleArray.begin() + index, module);
            moduleArray[index] = module;
        }
        return moduleArray;
    }
    /*vector<int> DescParser::getConveyorArray() {
        int index = 0;
        size_t i = 0;
        string conveyorDescription, blockNumberString;
        stringstream * conveyorStream;
        vector<int> conveyorArray;

        getline( *inputWorkflowStream_, conveyorDescription );
        conveyorStream = new stringstream( conveyorDescription );
        while ( getline(*conveyorStream, blockNumberString, STANDART_DELIMETER) ) {
            index = atoi(blockNumberString.c_str());
            conveyorArray.insert(conveyorArray.end(), index);

            getline(*conveyorStream, blockNumberString, STANDART_DELIMETER); // ignoring "->"
            if ((blockNumberString != "->") && (!blockNumberString.empty())) {
                //exeption
            }
        }
        return conveyorArray;
    }*/
    vector<int> DescParser::getConveyorArray() {
        int index = 0;

        string conveyorDescription, blockNumberString;
        stringstream * conveyorStream;
        vector<int> conveyorArray;

        getline( *inputWorkflowStream_, conveyorDescription );
        conveyorStream = new stringstream( conveyorDescription );

        while (true) {
            index = getIndex(*conveyorStream);
            conveyorArray.insert(conveyorArray.end(), index);

            if ( !getline(*conveyorStream, blockNumberString, STANDART_DELIMETER) ) { // ignoring "->"
                break;
            }
            try {
                if ((blockNumberString != STANDART_CONVEYOR_DELIMETER) && (!blockNumberString.empty())) {
                   throw Exeptions :: INCORRECT_CONVEYOR;
                }
            }
            catch (Exeptions e){
                sendExeptionMessage(e);
                exit(1);
            }
        }
        return conveyorArray;
    }

}
