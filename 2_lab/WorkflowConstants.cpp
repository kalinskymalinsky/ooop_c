#include "WorkflowConstants.h"

namespace kalinskymalinsky_WorkflowConstants {
    std::map <Exeptions, string> exeptionDescriptionMap{
            {OUT_OF_RANGE, "Out-of-range indexation"},
            {INCORRECT_WORKFLOW, "Input workflow isn't correct"},
            {CANT_GET_INDEX, "Getting module index failed"},
            {INCORRECT_INDEX, "Using non-numeric module index"},
            {INCORRECT_CONVEYOR, "Conveyor description isn't correct"},
            {UNINITIALIZED_MODULE, "Initializing of workflow module failed"}
    };
    string getExeptionDescription(Exeptions const & e) {
        return exeptionDescriptionMap[e];
    }
    void sendExeptionMessage(Exeptions const & e) {
        std::cout << EXEPTION_MESSAGE << e << SPACE << getExeptionDescription(e) << std::endl;
    }
}