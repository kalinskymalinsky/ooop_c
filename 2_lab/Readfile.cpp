#include <fstream>
#include "Readfile.h"
#include "WorkflowConstants.h"

using kalinskymalinsky_WorkflowConstants::FILE_EXEPTION_MESSAGE;
using std::ifstream;


namespace kalinskymalinsky_Readfile {
    Readfile::Readfile(istream & moduleParameterStream) {
        moduleParameterStream >> inputFileName_;
    }
    void Readfile::doActionWithText(Text &text) {
        ifstream inputStream;
        string buffer;

        inputStream.exceptions( std::ifstream::failbit | std::ifstream::badbit );
        try {
            inputStream.open(inputFileName_);
            while(!inputStream.eof()) {
                getline(inputStream, buffer);
                text.pushLine(buffer);
            }
        }
        catch (std::ifstream::failure e) {
            std::cout << FILE_EXEPTION_MESSAGE << std::endl;
            exit(1);
        }
    }
}