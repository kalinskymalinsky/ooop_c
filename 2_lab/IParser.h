#ifndef INC_2_LAB_IPARSER_H
#define INC_2_LAB_IPARSER_H

#include <istream>
#include "Workflow.h"

using std::istream;
using kalinskymalinsky_Workflow::Workflow;

class IParser {
public:
    virtual Workflow * parse() = 0;
};

#endif //INC_2_LAB_IPARSER_H
