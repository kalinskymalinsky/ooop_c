#include "Replace.h"
#include "WorkflowConstants.h"
#include <sstream>

using kalinskymalinsky_WorkflowConstants::STANDART_DELIMETER;
using std::stringstream;

namespace kalinskymalinsky_Replace {
    void Replace::doActionWithText(Text & text) {
        size_t textSize = text.size();
        string buffer;
        stringstream  lineStream;

        for (size_t i = 0; i < textSize; i++) {
             buffer = text.getLine(i);
             lineStream.clear();
             lineStream << buffer;
             buffer = replaceInLine(lineStream);
             text.setLine(i, buffer);
        }
    }
    Replace::Replace( istream & moduleParameterStream ) {

        getline( moduleParameterStream, replaceableWord_, STANDART_DELIMETER);
        getline( moduleParameterStream, replacingWord_);
    }
    string Replace::replaceInLine(istream & line) {
        string workBuffer, saveBuffer;

        while ( !line.eof() ) {
            getline(line, workBuffer, STANDART_DELIMETER);
            if (workBuffer == replaceableWord_) {
                saveBuffer += replacingWord_;
            }
            else {
                saveBuffer += workBuffer;
            }
            saveBuffer += STANDART_DELIMETER;
        }
        saveBuffer.pop_back(); // removing \n
        return saveBuffer;
    }
}