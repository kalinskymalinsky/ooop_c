#include "Sort.h"
#include <algorithm>

namespace kalinskymalinsky_Sort {
    void Sort::doActionWithText(Text & text) {
        vector<string> sortedText;
        size_t textSize = text.size();

        for (size_t i = 0; i < textSize; i++) {
            sortedText.insert(sortedText.end(), text.getLine(i));
        }
        std::sort(sortedText.begin(), sortedText.end());

        text.clear();
        for (size_t i = 0; i < textSize; i++) {
            text.pushLine(sortedText[i]);
        }

    }
    Sort::Sort( istream & moduleParameterStream ) {}
}