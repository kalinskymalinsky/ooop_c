#include <fstream>
#include "WorkflowConstants.h"
#include "Writefile.h"
using std::ofstream;
using kalinskymalinsky_WorkflowConstants::FILE_EXEPTION_MESSAGE;

namespace kalinskymalinsky_Writefile {
    void Writefile::doActionWithText(Text & text) {
        ofstream outStream;

        outStream.exceptions( std::ifstream::failbit | std::ifstream::badbit );
        try {
            outStream.open(outputFileName_);
            for (size_t i = 0; i < text.size(); i++) {
                outStream << text.getLine(i) << std::endl;
            }
        }
        catch (std::ifstream::failure e) {
            std::cout << FILE_EXEPTION_MESSAGE << std::endl;
            exit(1);
        }


    }
    Writefile::Writefile( istream & moduleParameterStream ) {
        moduleParameterStream >> outputFileName_;
    }
}