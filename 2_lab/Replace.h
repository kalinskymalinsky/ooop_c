#ifndef INC_2_LAB_REPLACE_H
#define INC_2_LAB_REPLACE_H

#include "IWorker.h"

using kalinskymalinsky_IWorker::IWorker;
using std::istream;

namespace kalinskymalinsky_Replace {
    class Replace : public IWorker {
    public:
        explicit Replace( istream & moduleParameterStream );
        void doActionWithText(Text & text) override;

    private:
        string replaceableWord_;
        string replacingWord_;

        string replaceInLine(istream & line);
    };
}



#endif //INC_2_LAB_REPLACE_H
