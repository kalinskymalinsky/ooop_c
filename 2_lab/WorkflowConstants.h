#ifndef INC_2_LAB_WORKFLOWCONSTANTS_H
#define INC_2_LAB_WORKFLOWCONSTANTS_H

#include <string>
#include <iostream>
#include <map>
using std::string;

namespace kalinskymalinsky_WorkflowConstants {

    const char STANDART_DELIMETER = ' ';
    const string STANDART_CONVEYOR_DELIMETER = "->";
    const string UP_DESC_EDGE = "desc";
    const string DOWN_DESC_EDGE = "csed";
    const int DOWN_DESC_FLAG = -1;
    const string EXEPTION_MESSAGE = "Process aborted. Exeption #";
    const string MODULE_DESCRIPTION_DELIMETER = "=";
    const char SPACE = ' ';
    const char ZERO = '0';
    const string FILE_EXEPTION_MESSAGE = "Exception opening/reading file";
    enum Exeptions {OUT_OF_RANGE, INCORRECT_WORKFLOW, CANT_GET_INDEX,INCORRECT_INDEX, INCORRECT_CONVEYOR, UNINITIALIZED_MODULE };

    string getExeptionDescription(Exeptions const & e);
    void sendExeptionMessage(Exeptions const & e);
}

#endif //INC_2_LAB_WORKFLOWCONSTANTS_H
