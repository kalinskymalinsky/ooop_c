#ifndef INC_2_LAB_TEXT_H
#define INC_2_LAB_TEXT_H

#include <iostream>
#include <vector>

using std::string;
using std::ostream;
using std::vector;

namespace kalinskymalinsky_Text {
    class Text {
    public:
        Text();
        explicit Text( string const & fileName );
        void pushLine(string const & line);
        string getLine(size_t const & index);
        size_t size();
        void clear();
        void setLine( size_t const & index, string const & newLine );
    private:
        vector<string> textArray;
    };
};

#endif //INC_2_LAB_TEXT_H
