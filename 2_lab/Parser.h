#ifndef INC_2_LAB_PARSER_H
#define INC_2_LAB_PARSER_H

#include "IParser.h"
#include "Workflow.h"
#include <iostream>

using std::istream;
using std::string;
using kalinskymalinsky_Workflow::Workflow;

namespace kalinskymalinsky_DescParser {
    class DescParser : public IParser {
    public:
        DescParser( istream * inputWorkflowStream );
        DescParser();

        Workflow * parse() override;
        void setInputWorkflow(istream & newInputWorkflow);

        virtual ~DescParser() = default;
    private:
        istream * inputWorkflowStream_;

        void skipToDesc() const;

        int getIndex(istream & inputLine) const ;
        string getModuleDescription(istream & inputLine) const ;
        vector<IWorker *> getModuleArray() ;
        vector<int> getConveyorArray() ;
    };
}

#endif //INC_2_LAB_PARSER_H
