#ifndef INC_2_LAB_IWORKER_H
#define INC_2_LAB_IWORKER_H

#include "Text.h"

using kalinskymalinsky_Text::Text;

namespace kalinskymalinsky_IWorker {
    class IWorker {
    public:
        virtual void doActionWithText(Text &text) = 0;
    };
}

#endif //INC_2_LAB_IWORKER_H
