#include "Dump.h"
#include <fstream>
#include "WorkflowConstants.h"
using std::ofstream;
using kalinskymalinsky_WorkflowConstants::FILE_EXEPTION_MESSAGE;
namespace kalinskymalinsky_Dump {

    void Dump::doActionWithText(Text & text) {
        ofstream outStream;
        outStream.exceptions( std::ifstream::failbit | std::ifstream::badbit );
        try {
            outStream.open(outputFileName_);
            for (size_t i = 0; i < text.size(); i++) {
                outStream << text.getLine(i) << std::endl;
            }
        }
        catch (std::ofstream::failure e) {
            std::cout << FILE_EXEPTION_MESSAGE << std::endl;
            exit(1);
        }

    }
    Dump::Dump( istream & moduleParameterStream ) {
        moduleParameterStream >> outputFileName_;
    }
}