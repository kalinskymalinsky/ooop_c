#ifndef INC_2_LAB_SORT_H
#define INC_2_LAB_SORT_H

#include "IWorker.h"
#include <iostream>

using kalinskymalinsky_IWorker::IWorker;
using std::istream;

namespace kalinskymalinsky_Sort {
    class Sort : public IWorker {
    public:
        explicit Sort( istream & moduleParameterStream );
        void doActionWithText(Text & text) override;
    };
}

#endif //INC_2_LAB_SORT_H
