#ifndef INC_2_LAB_DUMP_H
#define INC_2_LAB_DUMP_H

#include "IWorker.h"

using kalinskymalinsky_IWorker::IWorker;
using std::istream;

namespace kalinskymalinsky_Dump {
    class Dump : public IWorker {
    public:
        explicit Dump( istream & moduleParameterStream );
        void doActionWithText(Text & text) override;

    private:
        string outputFileName_;
    };
}

#endif //INC_2_LAB_DUMP_H
