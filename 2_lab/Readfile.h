#ifndef INC_2_LAB_READFILE_H
#define INC_2_LAB_READFILE_H

#include "IWorker.h"
#include <iostream>

using std::istream;

using kalinskymalinsky_IWorker::IWorker;
namespace kalinskymalinsky_Readfile {
    class Readfile : public IWorker {
    public:
        explicit Readfile(istream & moduleParameterStream);
        void doActionWithText(Text &text) override;


    private:
        string inputFileName_;
    };
}

#endif //INC_2_LAB_READFILE_H
