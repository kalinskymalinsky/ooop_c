#ifndef INC_2_LAB_WORKFLOW_H
#define INC_2_LAB_WORKFLOW_H

#include "IWorker.h"
#include <vector>

using kalinskymalinsky_IWorker::IWorker;
using std::vector;

namespace kalinskymalinsky_Workflow {
    class Workflow {
    public:
        Workflow(vector<IWorker *> & moduleArray, vector<int> & conveyorArray) :
            moduleArray(moduleArray),
            conveyorArray(conveyorArray){};

        void execute(Text &text);
        static IWorker * getModule(string const & moduleDescription);
    private:
        vector<IWorker *> moduleArray;
        vector <int> conveyorArray;
    };
}

#endif //INC_2_LAB_WORKFLOW_H
