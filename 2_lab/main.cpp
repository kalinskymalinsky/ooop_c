#include <iostream>
#include <fstream>
#include "Workflow.h"
#include "Parser.h"

using kalinskymalinsky_DescParser::DescParser;
using  std::ifstream;
using std::istream;

int main() {
    istream * workflowFileName = new ifstream("workflow.txt");

    auto parser = new DescParser(workflowFileName);
    auto workflow = parser->parse();
    auto text = new Text();

    workflow->execute(*text);

    return 0;
}