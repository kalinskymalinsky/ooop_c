#include <sstream>
#include "Workflow.h"
#include "Text.h"
#include "IWorker.h"
#include "WorkflowConstants.h"
#include <map>
#include "Readfile.h"
#include "Writefile.h"
#include "Grep.h"
#include "Sort.h"
#include "Dump.h"
#include "Replace.h"
#include "WorkflowConstants.h"

using kalinskymalinsky_WorkflowConstants::STANDART_DELIMETER;
using std::istream;
using std::stringstream;
using std::map;
using kalinskymalinsky_Grep::Grep;
using kalinskymalinsky_Readfile::Readfile;
using kalinskymalinsky_Writefile::Writefile;
using  kalinskymalinsky_Sort::Sort;
using kalinskymalinsky_Dump::Dump;
using kalinskymalinsky_Replace::Replace;
using kalinskymalinsky_WorkflowConstants::Exeptions;
using kalinskymalinsky_WorkflowConstants::sendExeptionMessage;

namespace kalinskymalinsky_Workflow {
    enum WorkflowModules {readfile = 1, writefile, sort, replace, grep, dump};
    std::map <std::string, WorkflowModules > modules{
            {"readfile", readfile},
            {"writefile", writefile},
            {"sort", sort},
            {"replace", replace},
            {"grep", grep},
            {"dump", dump}
    };

    void Workflow::execute( Text & text ) {
        for (size_t i = 0; i < conveyorArray.size(); i++ ) {
            try {
                if (conveyorArray[i] >= moduleArray.size()) {
                    throw Exeptions :: INCORRECT_CONVEYOR;
                }
                moduleArray[conveyorArray[i]]->doActionWithText( text );
            }
            catch (Exeptions e) {
                   sendExeptionMessage(e);
                   exit(1);
            }
        }
    }
    IWorker * Workflow::getModule(string const & moduleDescription) {
        string moduleName;
        string moduleParameters;
        istream * parameterStream;
        IWorker * module;

        stringstream * inputDescription;

        inputDescription = new stringstream (moduleDescription);
        getline(*inputDescription, moduleName, STANDART_DELIMETER );
        getline(*inputDescription, moduleParameters);

        parameterStream = new stringstream(moduleParameters);
        switch (modules[moduleName]) {
            case readfile:
                module = new Readfile(*parameterStream);
                return module;
            case writefile:
                module = new Writefile(*parameterStream);
                return module;
            case grep:
                module = new Grep(*parameterStream);
                return module;
            case sort:
                module = new Sort(*parameterStream);
                return module;
            case dump:
                module = new Dump(*parameterStream);
                return module;
            case replace:
                module = new Replace(*parameterStream);
                return module;
            default:
                return NULL;
        }
    }


}