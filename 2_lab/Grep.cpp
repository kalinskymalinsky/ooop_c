#include "Grep.h"
#include "WorkflowConstants.h"
#include <iostream>

using std::getline;
using kalinskymalinsky_WorkflowConstants::STANDART_DELIMETER;
using std::stringstream;

namespace kalinskymalinsky_Grep {
    void Grep::doActionWithText(Text & text) {
        Text newText;
        string buffer;
        stringstream stream;

        for (size_t i = 0 ; i < text.size(); i++) {
            buffer = text.getLine(i);
            stream.clear();
            stream << buffer;
            if (isFoundInLine(stream)) {
                newText.pushLine(buffer);
            }
        }
        text = newText;
    }
    Grep::Grep( istream & moduleParameterStream ) {
        moduleParameterStream >> regularExpression_;
    }
    bool Grep::isFoundInLine(istream & line) {
        string buffer;
        while ( !line.eof()) {
            getline(line, buffer, STANDART_DELIMETER);
            if (buffer == regularExpression_) {
                return true;
            }
        }
        return false;
    }
}