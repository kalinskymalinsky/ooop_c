#ifndef INC_2_LAB_WRITEFILE_H
#define INC_2_LAB_WRITEFILE_H

#include "IWorker.h"

using kalinskymalinsky_IWorker::IWorker;
using std::istream;

namespace kalinskymalinsky_Writefile {
    class Writefile : public IWorker {
    public:
        explicit Writefile( istream & moduleParameterStream );
        void doActionWithText(Text & text) override;

    private:
        string outputFileName_;
    };
}

#endif //INC_2_LAB_WRITEFILE_H
