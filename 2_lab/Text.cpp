#include <fstream>
#include "Text.h"
#include "WorkflowConstants.h"
using std::ifstream;
using std::getline;
using kalinskymalinsky_WorkflowConstants::EXEPTION_MESSAGE;
using kalinskymalinsky_WorkflowConstants::Exeptions;
using kalinskymalinsky_WorkflowConstants::sendExeptionMessage;


namespace kalinskymalinsky_Text {
    void Text::pushLine(string const & line) {
        vector<string> :: iterator it;
        it = textArray.end();
        textArray.insert(it, line);
    }
    Text::Text( string const & fileName ) {
        ifstream fin(fileName.c_str());
        string buffer;

        for ( int i = 0; getline(fin, buffer); i++ ) {
            pushLine(buffer);
        }
    }

    Text::Text( ) {}

    string Text::getLine(size_t const & index) {
        string line;
        try {
            if (index > textArray.size()) {
                throw Exeptions :: OUT_OF_RANGE;
            }
            line = textArray[index];
        }
        catch (Exeptions e) {
            sendExeptionMessage(e);
            exit(1);
        }
        return line;
    }

    size_t Text::size() {
        return textArray.size();
    }
    void Text::clear() {
        textArray.clear();
    }
    void Text::setLine( size_t const & index, string const & newLine ) {
        try{
            if (index >= textArray.capacity()) {
                throw Exeptions :: OUT_OF_RANGE;
            }
            textArray[index] =  newLine;
        }
        catch(Exeptions e ) {
            sendExeptionMessage(e);
            exit(1);
        }
    }
};