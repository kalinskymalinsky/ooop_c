#ifndef INC_2_LAB_GREP_H
#define INC_2_LAB_GREP_H

#include "IWorker.h"
#include <sstream>

using kalinskymalinsky_IWorker::IWorker;
using std::istream;

namespace kalinskymalinsky_Grep {
    class Grep : public IWorker {
    public:
        explicit Grep(istream &moduleParameterStream);

        void doActionWithText(Text &text) override;

    private:
        string regularExpression_;
        bool isFoundInLine(istream & line);
    };
}



#endif //INC_2_LAB_GREP_H
