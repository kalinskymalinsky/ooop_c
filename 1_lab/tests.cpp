#include <gtest/gtest.h>
#include "TritSet.h"

namespace kalinskymalinsky_TritSet_TestConstants {
    const int STANDART_SET_INDEX = 200;
    const int LESS_STANDART_SET_INDEX = 100;
    const int MUCH_LESS_STANDART_SET_INDEX = 10;
    const int MORE_STANDART_SET_INDEX = 900;
    const int MUCH_MORE_STANDART_SET_INDEX = 100000;
    const int BIGGEST_SET_INDEX = 200000;
    const int STANDART_SET_SIZE = 13;
    const int LESS_STANDART_SET_SIZE = 7;
    const int MORE_STANDART_SET_SIZE = 57;
    const int MUCH_MORE_STANDART_SET_SIZE = 6251;
    const int DEFAULT_TEST_INDEX = 1;
};

using namespace kalinskymalinsky_TritSet_TestConstants;
using namespace kalinskymalinsky_TritSet;
using namespace kalinskymalinsky_TritValues;
using kalinskymalinsky_TritConstants::NUM_OF_TRITS_IN_UINT;

TEST(TritSet_Indexation_operator_tests, Modification_test) {
    TritSet set(MUCH_LESS_STANDART_SET_INDEX);

    ASSERT_TRUE( set[DEFAULT_TEST_INDEX] == set[STANDART_SET_INDEX] );
    ASSERT_TRUE( set[DEFAULT_TEST_INDEX] == Unknown );

    set[DEFAULT_TEST_INDEX] = False;

    ASSERT_TRUE( set[DEFAULT_TEST_INDEX] == False );

    set[DEFAULT_TEST_INDEX] = Unknown;

    ASSERT_TRUE( set[DEFAULT_TEST_INDEX] == Unknown );

    set[DEFAULT_TEST_INDEX] = True;

    ASSERT_TRUE( set[DEFAULT_TEST_INDEX] == True );
    ASSERT_TRUE( set[DEFAULT_TEST_INDEX] != set[STANDART_SET_INDEX] );

    set[STANDART_SET_INDEX] = set[DEFAULT_TEST_INDEX];

    ASSERT_TRUE( set[STANDART_SET_INDEX] == True );
    ASSERT_TRUE( set[STANDART_SET_INDEX] == set[DEFAULT_TEST_INDEX] );
}

TEST(TritSet_Indexation_operator_tests, Resize_test) {
    TritSet set(LESS_STANDART_SET_INDEX);
    size_t allocLength = set.capacity();

    set[MUCH_MORE_STANDART_SET_INDEX] = Unknown;
    ASSERT_TRUE(allocLength == set.capacity());
    EXPECT_TRUE( set[BIGGEST_SET_INDEX] != True );
    ASSERT_TRUE(allocLength == set.capacity());

    set[LESS_STANDART_SET_INDEX - 1] = False;
    ASSERT_TRUE(allocLength == set.capacity());
    set[LESS_STANDART_SET_INDEX - 1] = Unknown;
    ASSERT_TRUE(allocLength == set.capacity());

    set[MUCH_MORE_STANDART_SET_INDEX] = True;
    EXPECT_TRUE(allocLength < set.capacity());
}

TEST(TritSet_Assignment_operator_tests, Value_to_proxy_test) {
    TritSet set(LESS_STANDART_SET_INDEX);

    set[MORE_STANDART_SET_INDEX] = True;
    ASSERT_TRUE(set[MORE_STANDART_SET_INDEX] == True);

    set[MORE_STANDART_SET_INDEX] = Unknown;
    ASSERT_TRUE(set[MORE_STANDART_SET_INDEX] == Unknown);

    set[MORE_STANDART_SET_INDEX] = False;
    ASSERT_TRUE(set[MORE_STANDART_SET_INDEX] == False);

    for ( int i = 0; i < LESS_STANDART_SET_INDEX; i++ ) {
        set[i] = True;
    }
    for ( int i = 0; i < LESS_STANDART_SET_INDEX; i++ ) {
        ASSERT_TRUE(set[i] == True);
    }
}

TEST(TritSet_Assignment_operator_tests, Proxy_to_proxy_test) {
    TritSet set(LESS_STANDART_SET_INDEX);
    set[MORE_STANDART_SET_INDEX] = True;

    set[MORE_STANDART_SET_INDEX] = set[MORE_STANDART_SET_INDEX];
    ASSERT_TRUE(set[MORE_STANDART_SET_INDEX] == set[MORE_STANDART_SET_INDEX]);
    ASSERT_TRUE(set[MORE_STANDART_SET_INDEX] == True);

    set[MORE_STANDART_SET_INDEX] = set[DEFAULT_TEST_INDEX];
    ASSERT_TRUE(set[MORE_STANDART_SET_INDEX] == Unknown);

    set[MORE_STANDART_SET_INDEX] = True;
    set[DEFAULT_TEST_INDEX] = set[MORE_STANDART_SET_INDEX];
    ASSERT_TRUE(set[DEFAULT_TEST_INDEX] == True);
}

TEST(TritSet_Cast_to_TritValues_operator_tests, Cast_test) {
    TritSet set(LESS_STANDART_SET_INDEX);
    TritValues temp = Unknown;

    set[MORE_STANDART_SET_INDEX] = True;
    temp = set[MORE_STANDART_SET_INDEX];
    ASSERT_TRUE(temp == True);
}

TEST(TritSet_AND_operator_tests, Set_test) {
    TritSet set1(STANDART_SET_INDEX);
    TritSet set2(LESS_STANDART_SET_INDEX);
    TritSet set3(MUCH_LESS_STANDART_SET_INDEX);

    for ( int i = 0; i < STANDART_SET_INDEX; i++ ) {
        set1[i] = True;
    }
    set3 = set1 & set2;
    ASSERT_TRUE( set3.length() == STANDART_SET_INDEX );
    for ( int i = 0; i < STANDART_SET_INDEX; i++ ) {
        ASSERT_TRUE(set3[i] == Unknown);
    }

    for ( int i = 0; i < STANDART_SET_INDEX; i++ ) {
        set2[i] = False;
    }
    set1 = set1 & set2;
    ASSERT_TRUE( set3.length() == STANDART_SET_INDEX );
    for ( int i = 0; i < STANDART_SET_INDEX; i++ ) {
        ASSERT_TRUE(set3[i] == False);
    }

}

TEST(TritSet_AND_operator_tests, Value_test) {
    TritSet set1(STANDART_SET_INDEX);

    for ( int i = 0; i < STANDART_SET_INDEX; i++ ) {
        set1[i] = True;
    }
    set1 = set1 & Unknown;
    ASSERT_TRUE( set1.length() == 0 );
    ASSERT_TRUE( set1.capacity() == STANDART_SET_SIZE );
    for ( int i = 0; i < STANDART_SET_INDEX; i++ ) {
        ASSERT_TRUE(set1[i] == Unknown);
    }

    for ( int i = 0; i < STANDART_SET_INDEX; i++ ) {
        set1[i] = True;
    }
    set1 = set1 & True;
    ASSERT_TRUE( set1.length() == STANDART_SET_INDEX );
    ASSERT_TRUE( set1.capacity() == STANDART_SET_SIZE );
    for ( int i = 0; i < STANDART_SET_INDEX; i++ ) {
        ASSERT_TRUE(set1[i] == True);
    }
}

TEST(TritSet_OR_operator_tests, Set_test) {
    TritSet set1(STANDART_SET_INDEX);
    TritSet set2(LESS_STANDART_SET_INDEX);
    TritSet set3(MUCH_LESS_STANDART_SET_INDEX);

    for ( int i = 0; i < STANDART_SET_INDEX; i++ ) {
        set1[i] = True;
    }
    set3 = set1 | set2;

    ASSERT_TRUE( set3.length() == STANDART_SET_INDEX );
    ASSERT_TRUE( set1.capacity() == STANDART_SET_SIZE );
    for ( int i = 0; i < STANDART_SET_INDEX; i++ ) {
        ASSERT_TRUE(set3[i] == True);
    }

    for ( int i = 0; i < STANDART_SET_INDEX; i++ ) {
        set1[i] = Unknown;
    }
    for ( int i = 0; i < STANDART_SET_INDEX; i++ ) {
        set2[i] = False;
    }
    set1 = set1 | set2;
    ASSERT_TRUE( set1.length() == 0 );
    ASSERT_TRUE( set1.capacity() == STANDART_SET_SIZE );
    for ( int i = 0; i < STANDART_SET_INDEX; i++ ) {
        ASSERT_TRUE(set3[i] == Unknown);
    }

}

TEST(TritSet_OR_operator_tests, Value_test) {
    TritSet set1(STANDART_SET_INDEX);

    for ( int i = 0; i < STANDART_SET_INDEX; i++ ) {
        set1[i] = True;
    }
    set1 = set1 | Unknown;
    ASSERT_TRUE( set1.length() == STANDART_SET_INDEX );
    ASSERT_TRUE( set1.capacity() == STANDART_SET_SIZE );
    for ( int i = 0; i < STANDART_SET_INDEX; i++ ) {
        ASSERT_TRUE(set1[i] == True);
    }

    for ( int i = 0; i < STANDART_SET_INDEX; i++ ) {
        set1[i] = True;
    }
    set1 = set1 | True;
    ASSERT_TRUE( set1.length() == STANDART_SET_INDEX );
    ASSERT_TRUE( set1.capacity() == STANDART_SET_SIZE );
    for ( int i = 0; i < STANDART_SET_INDEX; i++ ) {
        ASSERT_TRUE(set1[i] == True);
    }
}

TEST(TritSet_CAST_to_TritValues_operator_tests, Value_test) {
    TritSet set(STANDART_SET_INDEX);

    set[DEFAULT_TEST_INDEX] = True;
    ASSERT_TRUE( set[DEFAULT_TEST_INDEX] == True );

    set[DEFAULT_TEST_INDEX] = set[DEFAULT_TEST_INDEX + 1];
    ASSERT_TRUE( set[DEFAULT_TEST_INDEX] == Unknown );
}

TEST(Shrink_tests, Resize_test) {

    TritSet set(STANDART_SET_INDEX);
    size_t allocLength = set.capacity();

    set[MUCH_MORE_STANDART_SET_INDEX] = True;
    allocLength = set.capacity();
    set.shrink();
    ASSERT_TRUE(allocLength == set.capacity());

    set[MUCH_MORE_STANDART_SET_INDEX] = Unknown;
    allocLength = set.capacity();
    set.shrink();
    ASSERT_TRUE(allocLength > set.capacity());

    set[MUCH_MORE_STANDART_SET_INDEX] = True;
    set[MORE_STANDART_SET_INDEX] = True;

    set[MUCH_MORE_STANDART_SET_INDEX] = Unknown;
    set.shrink();
    ASSERT_TRUE(set[MORE_STANDART_SET_INDEX] == True);
}

TEST(Capacity_tests, Value_test) {
    TritSet set1(NUM_OF_TRITS_IN_UINT);
    TritSet set2(NUM_OF_TRITS_IN_UINT + 1);

    ASSERT_TRUE(set1.capacity() == 1);
    ASSERT_TRUE(set2.capacity() == 2);
}

TEST(Capacity_tests, Dynamic_test) {
    TritSet set(STANDART_SET_INDEX);

    ASSERT_TRUE(set.capacity() == STANDART_SET_SIZE);

    set[MORE_STANDART_SET_INDEX] = True;
    ASSERT_TRUE(set.capacity() == MORE_STANDART_SET_SIZE);

    set[MORE_STANDART_SET_INDEX] = Unknown;
    ASSERT_TRUE(set.capacity() == MORE_STANDART_SET_SIZE);

    set.shrink();
    ASSERT_TRUE(set.capacity() == STANDART_SET_SIZE);

    set[MUCH_MORE_STANDART_SET_INDEX - 1] = True;
    ASSERT_TRUE(set.capacity() == MUCH_MORE_STANDART_SET_SIZE - 1);
    ASSERT_TRUE(set[MUCH_MORE_STANDART_SET_INDEX - 1] == True);

    set[MUCH_MORE_STANDART_SET_INDEX] = True;
    ASSERT_TRUE(set.capacity() == MUCH_MORE_STANDART_SET_SIZE);
    ASSERT_TRUE(set[MUCH_MORE_STANDART_SET_INDEX] == True);
}

TEST(Cardinality_tests, Value_test) {
    TritSet set(STANDART_SET_INDEX);

    ASSERT_TRUE(set.cardinality(False) == 0);
    ASSERT_TRUE(set.cardinality(Unknown) == 0);
    ASSERT_TRUE(set.cardinality(True) == 0);

    for ( int i = 0; i < STANDART_SET_INDEX - 1; i++ ) {
        set[i] = True;
    }
    ASSERT_TRUE(set.cardinality(False) == 0);
    ASSERT_TRUE(set.cardinality(Unknown) == 0);
    ASSERT_TRUE(set.cardinality(True) == STANDART_SET_INDEX - 1);


    set[199] = True;
    ASSERT_TRUE(set.cardinality(False) == 0);
    ASSERT_TRUE(set.cardinality(Unknown) == 0);
    ASSERT_TRUE(set.cardinality(True) == STANDART_SET_INDEX);

    set[198] = Unknown;
    ASSERT_TRUE(set.cardinality(False) == 0);
    ASSERT_TRUE(set.cardinality(Unknown) == 1);
    ASSERT_TRUE(set.cardinality(True) == STANDART_SET_INDEX - 1);

    set[198] = False;
    ASSERT_TRUE(set.cardinality(False) == 1);
    ASSERT_TRUE(set.cardinality(Unknown) == 0);
    ASSERT_TRUE(set.cardinality(True) == STANDART_SET_INDEX - 1);
}

TEST(Cardinality_map_tests, Value_test) {
    TritSet set(STANDART_SET_INDEX);
    std::unordered_map <TritValues, int, std::hash<int> > cardinalityMap;

    cardinalityMap = set.cardinality();
    ASSERT_TRUE(cardinalityMap[False] == 0);
    ASSERT_TRUE(cardinalityMap[Unknown] == 0);
    ASSERT_TRUE(cardinalityMap[True] == 0);

    for ( int i = 0; i < STANDART_SET_INDEX - 1; i++ ) {
        set[i] = True;
    }
    cardinalityMap = set.cardinality();
    ASSERT_TRUE(cardinalityMap[False] == 0);
    ASSERT_TRUE(cardinalityMap[Unknown] == 0);
    ASSERT_TRUE(cardinalityMap[True] == STANDART_SET_INDEX - 1);

    set[STANDART_SET_INDEX - 1] = True;
    cardinalityMap = set.cardinality();
    ASSERT_TRUE(cardinalityMap[False] == 0);
    ASSERT_TRUE(cardinalityMap[Unknown] == 0);
    ASSERT_TRUE(cardinalityMap[True] == STANDART_SET_INDEX);

    set[STANDART_SET_INDEX - 2] = Unknown;
    cardinalityMap = set.cardinality();
    ASSERT_TRUE(cardinalityMap[False] == 0);
    ASSERT_TRUE(cardinalityMap[Unknown] == 1);
    ASSERT_TRUE(cardinalityMap[True] == STANDART_SET_INDEX - 1);

    set[STANDART_SET_INDEX - 2] = False;
    cardinalityMap = set.cardinality();
    ASSERT_TRUE(cardinalityMap[False] == 1);
    ASSERT_TRUE(cardinalityMap[Unknown] == 0);
    ASSERT_TRUE(cardinalityMap[True] == STANDART_SET_INDEX - 1);
}

TEST(Trim_tests, Resize_test) {
    TritSet set(STANDART_SET_INDEX);
    size_t allocLength = set.capacity();

    for ( int i = 0; i < STANDART_SET_INDEX; i++ ) {
        set[i] = True;
    }
    set.trim(LESS_STANDART_SET_INDEX);
    ASSERT_TRUE(allocLength > set.capacity());
    ASSERT_TRUE(set.capacity() == LESS_STANDART_SET_SIZE);
    for ( int i = 0; i < LESS_STANDART_SET_INDEX; i++ ) {
        ASSERT_TRUE(set[i] == True);
    }
    for ( int i = LESS_STANDART_SET_INDEX; i < STANDART_SET_INDEX; i++ ) {
        ASSERT_TRUE(set[i] == Unknown);
    }
}

TEST(Length_tests, Value_test) {
    TritSet set(STANDART_SET_INDEX);
    ASSERT_TRUE(set.length() == 0);

    set[STANDART_SET_INDEX - 1] = True;
    ASSERT_TRUE(set.length() == STANDART_SET_INDEX);

    set[STANDART_SET_INDEX - 1] = Unknown;
    ASSERT_TRUE(set.length() == 0);

    for ( int i = 0; i < STANDART_SET_INDEX; i++ ) {
        set[i] = True;
    }
    ASSERT_TRUE(set.length() == STANDART_SET_INDEX);

    set[STANDART_SET_INDEX - 1] = Unknown;
    ASSERT_TRUE(set.length() == STANDART_SET_INDEX - 1);
}


TEST(TritValues_AND_operator_tests, Value_test) {
    TritValues value = Unknown;
    ASSERT_TRUE( value == Unknown );

    value = False & False;
    ASSERT_TRUE( value == False );

    value = False & Unknown;
    ASSERT_TRUE( value == False );

    value = False & True;
    ASSERT_TRUE( value == False );

    value = Unknown & False;
    ASSERT_TRUE( value == False );

    value = Unknown & Unknown;
    ASSERT_TRUE( value == Unknown );

    value = Unknown & True;
    ASSERT_TRUE( value == Unknown );

    value = True & False;
    ASSERT_TRUE( value == False );

    value = True & Unknown;
    ASSERT_TRUE( value == Unknown );

    value = True & True;
    ASSERT_TRUE( value == True );
}

TEST(TritValues_OR_operator_tests, Value_test) {
    TritValues value = Unknown;
    ASSERT_TRUE( value == Unknown );

    value = False | False;
    ASSERT_TRUE( value == False );

    value = False | Unknown;
    ASSERT_TRUE( value == Unknown );

    value = False | True;
    ASSERT_TRUE( value == True );

    value = Unknown | False;
    ASSERT_TRUE( value == Unknown );

    value = Unknown | Unknown;
    ASSERT_TRUE( value == Unknown );

    value = Unknown | True;
    ASSERT_TRUE( value == True );

    value = True | False;
    ASSERT_TRUE( value == True );

    value = True | Unknown;
    ASSERT_TRUE( value == True );

    value = True | True;
    ASSERT_TRUE( value == True );
}