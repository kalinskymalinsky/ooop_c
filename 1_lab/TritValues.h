#pragma once
#ifndef INC_1_LAB_NEW_TRITVALUES_H
#define INC_1_LAB_NEW_TRITVALUES_H

#include <iostream>
namespace kalinskymalinsky_TritConstants {
    const uint32_t SIZE_OF_BYTE = 8;
    const uint32_t SIZE_OF_TRIT = 2;
    const uint32_t TRIT_MASK = 3;
    const uint32_t NUM_OF_TRITS_IN_UINT = SIZE_OF_BYTE * sizeof(uint32_t) / SIZE_OF_TRIT;
    const unsigned char BYTE_UNKNOWN_MASK = 0xAA; //1010 1010 == Unknown Unknown Unknown Unknown
    const uint32_t LENGTH_SHIFT = 1;
    const int32_t DEFAULT_LAST_INITIALIZED_TRIT_INDEX = 0;
    const int32_t POSITION_TO_SIZE_SHIFT = 1;
    const int32_t SUBPOSITION_SHIFT = 1;
};

namespace kalinskymalinsky_TritValues {
    enum TritValues {
        False, Unknown = 2, True
    }; // False = 00, Unknown = 10, True = 11

    std::ostream &operator<<(std::basic_ostream<char> &os, TritValues trit);

    TritValues operator&(TritValues const &first, TritValues const &second);

    TritValues operator|(TritValues const &first, TritValues const &second);

    TritValues operator~(TritValues const & operand);

}

#endif //INC_1_LAB_NEW_TRITVALUES_H
