#include "TritSet.h"
#include <math.h>

using kalinskymalinsky_TritConstants::NUM_OF_TRITS_IN_UINT;
using kalinskymalinsky_TritConstants::TRIT_MASK;
using kalinskymalinsky_TritConstants::SIZE_OF_BYTE;
using kalinskymalinsky_TritConstants::BYTE_UNKNOWN_MASK;
using kalinskymalinsky_TritConstants::LENGTH_SHIFT;
using kalinskymalinsky_TritConstants::DEFAULT_LAST_INITIALIZED_TRIT_INDEX;
using kalinskymalinsky_TritConstants::POSITION_TO_SIZE_SHIFT;
using kalinskymalinsky_TritConstants::SIZE_OF_TRIT;
using kalinskymalinsky_TritConstants::SUBPOSITION_SHIFT;
using kalinskymalinsky_TritValues::Unknown;

namespace kalinskymalinsky_TritSet {

    TritSet::TritSet(uint32_t numberOfTrits) {
        tritArraySize_ = ( size_t ) ceil(( double ) numberOfTrits / NUM_OF_TRITS_IN_UINT);
        startTritArraySize_ = tritArraySize_;
        lastInitializedTritIndex_ = DEFAULT_LAST_INITIALIZED_TRIT_INDEX; // Index
        tritArray = new uint32_t[tritArraySize_];
        setTritArrayDefault();
    }

    size_t TritSet::getPosition(size_t const &index) {
        return index / NUM_OF_TRITS_IN_UINT;
    }

    size_t TritSet::getSubPosition(size_t const &index) {
        return index % NUM_OF_TRITS_IN_UINT;
    }

    TritValues TritSet::getTritValue(size_t const &index) const {
        uint32_t value = 0;
        uint32_t mask = TRIT_MASK;
        mask <<= getShift(index);
        value = this->tritArray[this->getPosition(index)] & mask;
        value >>= getShift(index);
        return ( TritValues ) value;
    }

    int TritSet::getShift(size_t const &index) {
        return (NUM_OF_TRITS_IN_UINT - getSubPosition(index) - SUBPOSITION_SHIFT) * SIZE_OF_TRIT;
    }

    TritSet::ProxyTrit TritSet::operator[](size_t const &index) {
        if (getPosition(index) < tritArraySize_) {
            return TritSet::ProxyTrit( (*this),  index, this->getTritValue(index));
        } else {
            return TritSet::ProxyTrit((*this), index, Unknown);
        }
    }

    TritSet TritSet::operator&(TritSet & other) {
        this->resizeControl( other );
        for (size_t i = 0; i < this->tritArraySize_; i++) {
            this->tritArray[i] &= other.tritArray[i];
        }
        return (*this);
    }

    TritSet TritSet::operator&(TritValues const & value) {
        size_t edge = this->length();
        for (size_t i = 0; i < edge; i++) {
            (*this)[i] = (*this)[i] & value;
        }
        return (*this);
    }

    TritSet TritSet::operator|(TritSet & other) {
        this->resizeControl( other );
        for (size_t i = 0; i < this->tritArraySize_; i++) {
            this->tritArray[i] |= other.tritArray[i];
        }
        return (*this);
    }
    TritSet TritSet::operator|(TritValues const & value) {
        for (size_t i = 0; i < this->tritArraySize_; i++) {
            (*this)[i] = (*this)[i] | value;
        }
        return (*this);
    }

    TritSet TritSet::operator~() {
        TritValues temp;
        for ( size_t i = 0; i < length(); i++ ) {
            temp = getTritValue(i);
            setTritValue(i, ~temp);
        }
        return *this;
    }

    uint32_t TritSet::makeMask() {
        uint32_t mask = BYTE_UNKNOWN_MASK;
        for (int i = 1; i < sizeof(uint32_t); i++) {
            mask <<= SIZE_OF_BYTE;
            mask |= BYTE_UNKNOWN_MASK;
        }
        return mask;
    }

    void TritSet::setTritArrayDefault() {
        uint32_t mask = makeMask();
        for (size_t i = 0; i < tritArraySize_; i++) {
            tritArray[i] = mask;
        }
    }

    void TritSet::setTritArrayDefault( size_t startPosition) {
        uint32_t mask = makeMask();
        for (size_t i = startPosition; i < tritArraySize_; i++) {
            tritArray[i] = mask;
        }
    }

    void TritSet::setTritValue(size_t index, TritValues newValue) {
        if ( getPosition(index) < this->tritArraySize_ ) {
            this->tritArray[getPosition(index)] &= ~( TRIT_MASK << TritSet::getShift(index) ); // 1111'00'1111
            this->tritArray[getPosition(index)] |= ( newValue << TritSet::getShift(index) ); // 1111'10'1111
        }
    }

    size_t TritSet::capacity() {
        return this->tritArraySize_;
    }

    void TritSet::shrink() {
        size_t newSize = convertIndexToArraySize(lastInitializedTritIndex_);
        if (newSize < startTritArraySize_) {
            newSize = startTritArraySize_;
        }
        this->reallocTritSet(newSize);
    }

    size_t TritSet::cardinality(TritValues value) const {

        uint32_t cnt = 0;
        for (size_t i = 0; i < length(); i++) {
            if (this->getTritValue(i) == value) {
                cnt++;
            }
        }
        return cnt;
    }

    std::unordered_map<TritValues, int, std::hash<int> > TritSet::cardinality() {
        std::unordered_map<TritValues, int, std::hash<int> > counterMap;
        for (uint32_t i = 0; i <length(); i++) {
            counterMap[this->getTritValue(i)]++;
        }
        return counterMap;
    }

    void TritSet::trim(size_t lastIndex) {
        int isItFirst;

        getSubPosition(lastIndex) == 0 ? isItFirst = 0 : isItFirst = 1;
        this->reallocTritSet(getPosition(lastIndex) + isItFirst);
        for (size_t i = lastIndex; i < lastIndex + NUM_OF_TRITS_IN_UINT; i++) {
            (*this)[i] = Unknown;
        }
        if (this->lastInitializedTritIndex_ >= lastIndex) {
            this->refreshLastInitializedTritIndex();
        }
    }

    size_t TritSet::length() const {
        if ( lastInitializedTritIndex_ == 0 ) {
            if ( getTritValue(lastInitializedTritIndex_) == Unknown ) {
                return lastInitializedTritIndex_;
            }
        }
        return lastInitializedTritIndex_ + LENGTH_SHIFT;
    }

    void TritSet::reallocTritSet( size_t newSize ) { // Can crush
        uint32_t * savedArray, * temp;
        size_t oldTritArraySize = tritArraySize_;

        reallocTritArray( tritArray, tritArraySize_, newSize);
        //savedArray = this->tritArray;
        //temp = new uint32_t [newSize];
        //this->tritArray = temp;
        //copyArray( tritArray, oldTritArraySize, savedArray );
        //delete [] savedArray;
        tritArraySize_ = newSize;
        setTritArrayDefault( oldTritArraySize );
        refreshLastInitializedTritIndex();
    }

    void TritSet::refreshLastInitializedTritIndex() {
        int32_t max_index = DEFAULT_LAST_INITIALIZED_TRIT_INDEX;

        for ( uint32_t i = 0; i < length(); i++ ) {
            if ( getTritValue(i) != Unknown ) {
                max_index = i;
            }
        }
        lastInitializedTritIndex_ = max_index;
    }

    size_t TritSet::convertIndexToArraySize( size_t const & index ) {
        return (index / NUM_OF_TRITS_IN_UINT) + POSITION_TO_SIZE_SHIFT;
    }

    void TritSet::resizeControl( TritSet & other ) {
        if (this->tritArraySize_ < other.tritArraySize_) {
            this->reallocTritSet(other.tritArraySize_);
        } else if (other.tritArraySize_ < this->tritArraySize_) {
            other.reallocTritSet(this->tritArraySize_);
        }
    }

    void TritSet::copyArray( uint32_t * & outArray, size_t const & outSize, uint32_t * &inArray, size_t const & inSize) {
        size_t size = outSize;
        if ( inSize < outSize ) size = inSize;
        for (size_t i = 0; i < size; i++ ) {
            outArray[i] = inArray[i];
        }
    }

    void TritSet::reallocTritArray(uint32_t *& tritArray, size_t &tritArraySize, size_t newSize) {
            uint32_t * temp = NULL;
            temp = tritArray;
            tritArray = new uint32_t [newSize];
            copyArray(tritArray, newSize, temp, tritArraySize );
            delete[] temp;
    }

    TritSet::~TritSet() = default;
};