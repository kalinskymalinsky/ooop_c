#include "TritValues.h"
#include <map>
#include <string>
#include <iostream>

using kalinskymalinsky_TritConstants::TRIT_MASK;

namespace kalinskymalinsky_TritValues {

    std::map <TritValues, std::string> outNames{
            {False,   "False"},
            {Unknown, "Unknown"},
            {True,    "True"}
    };

    std::ostream &operator<<(std::basic_ostream<char> &os, TritValues trit) {
        os << outNames[trit];
        return os;
    };

    TritValues operator&(TritValues const &first, TritValues const &second) {
        return ( TritValues ) (( uint32_t ) first & ( uint32_t ) second);
    }

    TritValues operator|(TritValues const &first, TritValues const &second) {
        return ( TritValues ) (( uint32_t ) first | ( uint32_t ) second);
    }

    TritValues operator~(TritValues const & operand) {
        uint32_t temp = operand;
        if ( temp != Unknown ) {
            return (TritValues)(temp ^ TRIT_MASK);
        }
        else return Unknown;
    }
};