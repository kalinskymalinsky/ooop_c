/*#include "Trit.h"
#include "TritSet.h"

Trit::Trit( uint32_t & arrayValue, uint32_t position, uint32_t subPosition, TritValues tritValue) : arrayValue_( arrayValue ), position_(position), subPosition_(subPosition), tritValue_(tritValue) {}

Trit & Trit::operator= ( TritValues const & value) {
    arrayValue_ &= ~( TRIT_MASK << TritSet::getShift( subPosition_ ) );
    arrayValue_ |= ( value << TritSet::getShift( subPosition_ ) );
    tritValue_ = value;
    return *this;
}
Trit & Trit::operator= ( Trit const & other) {
    arrayValue_ &= ~( TRIT_MASK << TritSet::getShift( subPosition_ ) );
    arrayValue_ |= ( other.tritValue_ << TritSet::getShift( subPosition_ ) );
    tritValue_ = other.tritValue_;
    return *this;
}
Trit::operator TritValues(){
    return this->tritValue_;
}*/
#include "TritSet.h"

using kalinskymalinsky_TritValues::TritValues;
using kalinskymalinsky_TritValues::Unknown;
using kalinskymalinsky_TritConstants::TRIT_MASK;


namespace kalinskymalinsky_TritSet {
    void TritSet::ProxyTrit::modifyTrit(TritValues newValue) {
        if (getPosition(this->index_) >= set.tritArraySize_) {
            if (newValue != Unknown) {
                set.reallocTritSet(convertIndexToArraySize(this->index_));
            } else {
                return;
            }
        }
        set.setTritValue(this->index_, newValue);

        if (newValue != Unknown) {
            if (set.lastInitializedTritIndex_ < ( int32_t ) this->index_) {
                set.lastInitializedTritIndex_ = this->index_;
            }
        } else {
            if (this->tritValue_ != Unknown) {
                set.refreshLastInitializedTritIndex();
            }
        }

        tritValue_ = newValue;
    }

    TritSet::ProxyTrit &TritSet::ProxyTrit::operator=(enum TritValues const &value) {
        modifyTrit(value);
        return *this;
    }

    TritSet::ProxyTrit &TritSet::ProxyTrit::operator=(ProxyTrit const &other) {
        modifyTrit(other.tritValue_);
        return *this;
    }

    TritSet::ProxyTrit::operator enum TritValues() {
        return this->tritValue_;
    }

    TritSet::ProxyTrit::~ProxyTrit() = default;
};