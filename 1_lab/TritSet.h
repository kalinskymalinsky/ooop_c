#ifndef INC_1_LAB_NEW_TRITSET_H
#define INC_1_LAB_NEW_TRITSET_H

#include <iostream>
#include "TritValues.h"
#include <unordered_map>

using std::uint32_t;
using kalinskymalinsky_TritValues::TritValues;

namespace kalinskymalinsky_TritSet {
    class TritSet {
    public:
        explicit TritSet(uint32_t numberOfTrits);

        class ProxyTrit {
        public:
            explicit ProxyTrit( TritSet & set, size_t index, TritValues tritValue) :
                    set(set),
                    index_(index),
                    tritValue_(tritValue) {}

            ProxyTrit &operator=(TritValues const &value);

            ProxyTrit &operator=(ProxyTrit const &other);

            operator enum TritValues();

            virtual ~ProxyTrit();

        private:
            TritSet & set;
            size_t index_;
            TritValues tritValue_;

            void modifyTrit(TritValues newValue);

        };

        ProxyTrit operator[](size_t const &index);

        TritSet operator&(TritSet &other);

        TritSet operator&(TritValues const & value);

        TritSet operator|(TritSet &other);

        TritSet operator|(TritValues const & value);

        TritSet operator~();

        void shrink();

        size_t capacity();

        size_t cardinality(TritValues value) const;

        std::unordered_map<TritValues, int, std::hash<int> > cardinality();

        void trim(size_t lastIndex);

        size_t length() const;

        virtual ~TritSet();

    private:
        uint32_t * tritArray;
        size_t lastInitializedTritIndex_;
        size_t tritArraySize_;
        size_t startTritArraySize_;

        static size_t getPosition(size_t const &index);

        static size_t getSubPosition(size_t const &index);

        TritValues getTritValue(size_t const &index) const;

        static int getShift(size_t const &index);

        static size_t convertIndexToArraySize( size_t const & index );

        void setTritArrayDefault();

        void setTritArrayDefault(size_t startPosition);

        void setTritValue(size_t index, TritValues newValue);

        void reallocTritSet(size_t newSize);

        static uint32_t makeMask();

        void refreshLastInitializedTritIndex();

        void resizeControl( TritSet & other );

        static void copyArray( uint32_t * & outArray, size_t const & outSize, uint32_t * &inArray, size_t const & inSize);

        void reallocTritArray(uint32_t *&tritArray, size_t &tritArraySize, size_t newSize);
    };
};


#endif //INC_1_LAB_NEW_TRITSET_H
