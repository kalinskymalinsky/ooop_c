#ifndef INC_0_LAB_WRITER_H
#define INC_0_LAB_WRITER_H

#include <fstream>
#include <iostream>
#include "InverseMultimap.h"

using kalinskymalinsky_InverseMultimap::InverseMultimap;

namespace  kalinskymalinsky_Writer {
    using std::ofstream;

    class Writer {
    public:

        explicit Writer(char *outputName);

        void writeMapInCSV(InverseMultimap &counterMultimap, int numberOfWords);

        virtual ~Writer();

    private:
        ofstream fout;
    };
};
#endif //INC_0_LAB_WRITER_H
