#include <iostream>
#include <fstream>
#include "Writer.h"

using namespace std;

int main( int argc, char * argv[] ) {

    CounterMap counterTable;

    ifstream fin( argv[1] );
    if (!fin) {
        cout << "Input file not found" << endl;
        return 0;
    }

    Writer output( argv[2] );

    counterTable.parseAndCount( fin );
    InverseMultimap inverseCounterTable( counterTable );
    output.writeMapInCSV( inverseCounterTable, counterTable.getNumberOfWords() );

    fin.close();
    return 0;
}
