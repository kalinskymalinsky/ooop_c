
#include "Writer.h"

using std::cout;
using std::endl;
using kalinskymalinsky_CounterMapConstants::FREQUENCY_TO_PERCENT_MULTIPLIER;
using kalinskymalinsky_CounterMapConstants::COLUMN_DELIMITER;
using kalinskymalinsky_CounterMapConstants::PERCENT_SYMBOL;
using kalinskymalinsky_CounterMapConstants::NO_WORDS;
using kalinskymalinsky_CounterMapConstants::WORDS_NOT_FOUND_MESSAGE;

namespace kalinskymalinsky_Writer {

    Writer::Writer(char *outputName) {
        fout.open(outputName);
    }

    void Writer::writeMapInCSV(InverseMultimap &counterMultimap, int numberOfWords) {
        float frequency = 0;
        auto it = (counterMultimap.Multimap).end();
        if (numberOfWords == NO_WORDS) {
            cout << WORDS_NOT_FOUND_MESSAGE << endl;
            return;
        }
        do {
            it--; // Идём в обратном направлении, чтобы выводить частоту по убыванию
            frequency = ( float ) it->first / numberOfWords;
            frequency *= FREQUENCY_TO_PERCENT_MULTIPLIER; // Перевод частоты в %
            fout << it->second << COLUMN_DELIMITER << it->first << COLUMN_DELIMITER << frequency << PERCENT_SYMBOL << endl;
        } while (it != counterMultimap.Multimap.begin());
    }

    Writer::~Writer() {
        fout.close();
    }
};