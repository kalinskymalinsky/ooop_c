//
#ifndef INC_0_LAB_INVERSEMULTIMAP_H
#define INC_0_LAB_INVERSEMULTIMAP_H
#include "CounterMap.h"
#include <iostream>
#include <map>

using std::string;
using std::multimap;
using kalinskymalinsky_CounterMap::CounterMap;

namespace kalinskymalinsky_InverseMultimap {
    class InverseMultimap {
    public:
        multimap<int, string> Multimap;

        explicit InverseMultimap(CounterMap const &inputMap);
    };
};


#endif //INC_0_LAB_INVERSEMULTIMAP_H
